--DB result format
--result[2]["Login"]

--Colors variables | zmiennie z kolorami
c_red = "#D80000"
c_blue = "#3F3FFF"
c_green = "#03A803"
c_white = "#FFFFFF"
c_black = "#000000"
c_gray = "#878787"
c_yellow = "#FFFF00"
c_lime = "#9DFF00"
c_orange = "#FF9D00"
c_cyan = "#00B2A6"
c_pink = "#FF00FA"
c_purple = "#8501A3"

--Notification styles
s_INFO = 0
s_ERROR = 1
s_WARN = 2
s_SUCCESS = 3

--Account start values
acc_START_MONEY = 100