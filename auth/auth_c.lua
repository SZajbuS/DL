local screenW, screenH = guiGetScreenSize()

function initWindows()
	--Rules
        wdwRules = guiCreateWindow((screenW - 356) / 2, (screenH - 300) / 2, 356, 300, "Regulamin", false)
        guiWindowSetSizable(wdwRules, false)

        RulesLblRules = guiCreateLabel(10, 26, 336, 207, "1.\n2.\n3.\n4.\n5.\n6.\n", false, wdwRules)
        RulesBtnClose = guiCreateButton(10, 243, 336, 47, "Zamknij", false, wdwRules)
    --Login
        wdwLogin = guiCreateWindow((screenW - 319) / 2, (screenH - 232) / 2, 319, 232, "Zaloguj się", false)
        guiWindowSetMovable(wdwLogin, false)
        guiWindowSetSizable(wdwLogin, false)

        LoginLblLogin = guiCreateLabel(10, 26, 92, 44, "Login", false, wdwLogin)
        guiLabelSetHorizontalAlign(LoginLblLogin, "center", false)
        guiLabelSetVerticalAlign(LoginLblLogin, "center")
        LoginEdtLogin = guiCreateEdit(112, 26, 197, 44, "", false, wdwLogin)
        LoginLblPassword = guiCreateLabel(10, 80, 92, 44, "Hasło", false, wdwLogin)
        guiLabelSetHorizontalAlign(LoginLblPassword, "center", false)
        guiLabelSetVerticalAlign(LoginLblPassword, "center")
        LoginEdtPassword = guiCreateEdit(112, 80, 197, 44, "", false, wdwLogin)
        guiEditSetMasked(LoginEdtPassword, true)
        LoginLblError = guiCreateLabel(10, 134, 299, 24, "", false, wdwLogin)
        guiSetFont(LoginLblError, "default-bold-small")
        guiLabelSetColor(LoginLblError, 197, 0, 0)
        guiLabelSetHorizontalAlign(LoginLblError, "center", false)
        guiLabelSetVerticalAlign(LoginLblError, "center")
        LoginBtnLogin = guiCreateButton(10, 168, 299, 54, "Zaloguj", false, wdwLogin)
        LoginBtnBack = guiCreateButton(603, 510, 160, 31, "Wróc", false)  
    --Register
        wdwRegister = guiCreateWindow((screenW - 319) / 2, (screenH - 289) / 2, 319, 289, "Załóż konto", false)
        guiWindowSetMovable(wdwRegister, false)
        guiWindowSetSizable(wdwRegister, false)

        RegisterLblLogin = guiCreateLabel(10, 26, 92, 44, "Login", false, wdwRegister)
        guiLabelSetHorizontalAlign(RegisterLblLogin, "center", false)
        guiLabelSetVerticalAlign(RegisterLblLogin, "center")
        RegisterEdtLogin = guiCreateEdit(112, 26, 197, 44, "", false, wdwRegister)
        RegisterLblPassword = guiCreateLabel(10, 80, 92, 44, "Hasło", false, wdwRegister)
        guiLabelSetHorizontalAlign(RegisterLblPassword, "center", false)
        guiLabelSetVerticalAlign(RegisterLblPassword, "center")
        RegisterEdtPassword = guiCreateEdit(112, 80, 197, 44, "", false, wdwRegister)
        guiEditSetMasked(RegisterEdtPassword, true)
        RegisterLblRePassword = guiCreateLabel(10, 134, 92, 44, "Powtórz hasło", false, wdwRegister)
        guiLabelSetHorizontalAlign(RegisterLblRePassword, "center", false)
        guiLabelSetVerticalAlign(RegisterLblRePassword, "center")
        RegisterEdtRePassword = guiCreateEdit(112, 134, 197, 44, "", false, wdwRegister)
        guiEditSetMasked(RegisterEdtRePassword, true)
        RegisterLblError = guiCreateLabel(10, 188, 299, 24, "", false, wdwRegister)
        guiSetFont(RegisterLblError, "default-bold-small")
        guiLabelSetColor(RegisterLblError, 197, 0, 0)
        guiLabelSetHorizontalAlign(RegisterLblError, "center", false)
        guiLabelSetVerticalAlign(RegisterLblError, "center")
        RegisterBtnRegister = guiCreateButton(10, 222, 299, 54, "Zarejestruj", false, wdwRegister)
        RegisterBtnBack = guiCreateButton(603, 537, 160, 31, "Wróc", false) 
    --Select
        wdwSelect = guiCreateWindow((screenW - 219) / 2, (screenH - 224) / 2, 219, 224, "Witamy", false)
        guiWindowSetMovable(wdwSelect, false)
        guiWindowSetSizable(wdwSelect, false)

        SelectBtnLogin = guiCreateButton(10, 27, 199, 56, "Zaloguj się", false, wdwSelect)
        SelectBtnRegister = guiCreateButton(10, 93, 199, 56, "Załóż konto", false, wdwSelect)
        SelectBtnRules = guiCreateButton(10, 171, 199, 38, "Regulamin", false, wdwSelect)

    --Visibility
    	guiSetVisible(wdwSelect, false)
    	guiSetVisible(wdwRegister, false)
    	guiSetVisible(wdwLogin, false)
    	guiSetVisible(wdwRules, false)
    	guiSetVisible(LoginBtnBack, false)
    	guiSetVisible(RegisterBtnBack, false)

   	--GUI Clicks
   		--Login
   		addEventHandler("onClientGUIClick", LoginBtnLogin, clientSubmitLogin, false)
   		addEventHandler("onClientGUIClick", LoginBtnBack, clientLoginBack, false)
   		--Register
   		addEventHandler("onClientGUIClick", RegisterBtnRegister, clientSubmitRegister, false)
   		addEventHandler("onClientGUIClick", RegisterBtnBack, clientRegisterBack, false)
   		--Rules
   		addEventHandler("onClientGUIClick", RulesBtnClose, clientCloseRules, false)
   		--Select
   		addEventHandler("onClientGUIClick", SelectBtnLogin, clientSelectLogin, false)
   		addEventHandler("onClientGUIClick", SelectBtnRegister, clientSelectRegister, false)
   		addEventHandler("onClientGUIClick", SelectBtnRules, clientSelectRules, false)
end

function showBackground()
        dxDrawLine(417 - 1, 0 - 1, 417 - 1, 768, tocolor(217, 0, 0, 156), 1, false)
        dxDrawLine(949, 0 - 1, 417 - 1, 0 - 1, tocolor(217, 0, 0, 156), 1, false)
        dxDrawLine(417 - 1, 768, 949, 768, tocolor(217, 0, 0, 156), 1, false)
        dxDrawLine(949, 768, 949, 0 - 1, tocolor(217, 0, 0, 156), 1, false)
        dxDrawRectangle((screenW - 532) / 2, (screenH - 768) / 2, 532, 768, tocolor(0, 0, 0, 156), false)
        dxDrawText("drift#FF0000Legends", screenW * 0.3126, screenH * 0.0130, screenW * 0.6874, screenH * 0.1523, tocolor(255, 255, 255, 255), 3.50, "pricedown", "center", "top", false, false, false, true, false)
        dxDrawText("RESURRECTION", screenW * 0.5498, screenH * 0.1081, screenW * 0.6874, screenH * 0.1393, tocolor(255, 255, 255, 255), 0.69, "bankgothic", "left", "top", false, false, false, false, false)
end
addEventHandler("onClientRender", root, showBackground)
---------Resource start
function onResourceStart()
	initWindows()
	--SetCameraPos
	fadeCamera(true, 5)
	setCameraMatrix(690, -1177, 65, 1524, -1370, 233, 0, 80)
	setTime(0, 0)

	--ShowBackground
	setPlayerHudComponentVisible("all", false)
	showChat(false)

	--Show select window
	guiSetVisible(wdwSelect, true)
	showCursor(true)
	guiSetInputEnabled(true)
end
addEventHandler("onClientResourceStart", root, onResourceStart)

--Buttons manage
function clearLoginPassword()
guiSetText(LoginEdtPassword, "")
end

function clearRegisterPassword()
guiSetText(RegisterEdtPassword, "")
guiSetText(RegisterEdtRePassword, "")
end

--Login
function clientSubmitLogin(button, state)
	local Login = guiGetText(LoginEdtLogin)
	local Password = guiGetText(LoginEdtPassword)

	if button == "left" and state == "up" then
		if Login == "" or Password == "" then
			guiSetText(LoginLblError, "Wypełnij wszystkie pola")
			clearLoginPassword()
			return
		end

		triggerServerEvent("submitLogin", localPlayer, Login, md5(Password))
	end
end

function clientLoginBack(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwLogin, false)
		guiSetVisible(LoginBtnBack, false)
		guiSetVisible(wdwSelect, true)
	end
end

--Register
function clientSubmitRegister(button, state)
	local Login = guiGetText(RegisterEdtLogin)
	local Password = guiGetText(RegisterEdtPassword)
	local RePassword = guiGetText(RegisterEdtRePassword)

	if button == "left" and state == "up" then
		if Login == "" or Password == "" or RePassword == "" then
			guiSetText(RegisterLblError, "Wypełnij wszystkie pola")
			clearRegisterPassword()
			return
		elseif Password ~= RePassword then
			guiSetText(RegisterLblError, "Hasła nie są jednakowe")
			clearRegisterPassword()
			return
		end

		triggerServerEvent("submitRegister", localPlayer, Login, md5(Password))
	end
end

function clientRegisterBack(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwRegister, false)
		guiSetVisible(RegisterBtnBack, false)
		guiSetVisible(wdwSelect, true)
	end
end

--Rules
function clientCloseRules(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwRules, false)
		guiSetVisible(wdwSelect, true)
	end
end

--Select
function clientSelectLogin(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwSelect, false)
		guiSetVisible(wdwLogin, true)
		guiSetVisible(LoginBtnBack, true)
	end
end

function clientSelectRegister(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwSelect, false)
		guiSetVisible(wdwRegister, true)
		guiSetVisible(RegisterBtnBack, true)
	end
end

function clientSelectRules(button, state)
	if button == "left" and state == "up" then
		guiSetVisible(wdwSelect, false)
		guiSetVisible(wdwRules, true)
	end
end

--Register callbacks
function registerError(error)
	guiSetText(RegisterLblError, error)
	clearRegisterPassword()
end
addEvent("registerError", true)
addEventHandler("registerError", root, registerError)

function registerComplete()
	guiSetVisible(wdwRegister, false)
	guiSetVisible(wdwLogin, true)
end
addEvent("registerComplete", true)
addEventHandler("registerComplete", root, registerComplete)

--Login callbacks
function loginError(error)
	guiSetText(LoginLblError, error)
	clearLoginPassword()
end
addEvent("loginError", true)
addEventHandler("loginError", root, loginError)

function loginComplete()
	guiSetVisible(wdwLogin, false)
	guiSetVisible(LoginBtnBack, false)
	guiSetVisible(RegisterBtnBack, false)
	showCursor(false)
	guiSetInputEnabled(false)
	setPlayerHudComponentVisible("all", true)
	removeEventHandler("onClientRender", root, showBackground)
	showChat(true)
end
addEvent("loginComplete", true)
addEventHandler("loginComplete", root, loginComplete)