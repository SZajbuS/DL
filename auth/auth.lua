
local spawnpoints = {
	{2152.7, 1015.9, 10.8},
	{2026.8, 1916.9, 12.3},
	{-311.1, 1531.2, 75.4},
	{400.9, 2532.7, 16.5},
	{-2029.3, 143.5, 28.8},
	{-2397.2, 593, 132.6},
	{-2706.8999, 345.39999, 4.4},
	{1539.5, -1675.8, 13.5},
	{2495.5, -1684.3, 13.5},
	{2020.8, -2183.5, 13.5},
}

function submitRegister(Login, Password)
	if isAccountInDB(Login) == true then
		triggerClientEvent(source, "registerError", resourceRoot, "Login zajęty")
		return
	end

	local Serial = getPlayerSerial(source)

	addAccountToDB(Login, Password, Serial)
	triggerClientEvent(source, "registerComplete", resourceRoot)
end
addEvent("submitRegister", true)
addEventHandler("submitRegister", root, submitRegister)

function submitLogin(Login, Password)
	if isPasswordCorrect(Login, Password) == false then
		triggerClientEvent(source, "loginError", resourceRoot, "Hasło nieprawidłowe")
		return
	end

	triggerClientEvent(source, "loginComplete", resourceRoot)
	local rand = math.random(1, #spawnpoints)
	local spawnx = spawnpoints[rand][1]
	local spawny = spawnpoints[rand][2]
	local spawnz = spawnpoints[rand][3]
	spawnPlayer(source, spawnx, spawny, spawnz)
	setCameraTarget(source, source)

end
addEvent("submitLogin", true)
addEventHandler("submitLogin", root, submitLogin)