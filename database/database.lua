--dbConnect("mysql", "dbname=dl;host=localhos", "root", "")

db = nil
query = nil
result = nil

function connectDatabase()
	db = dbConnect("mysql", "dbname=dl;host=localhost", "root", "")

	if db then
		outputDebugString("Database connected")
	else
		outputDebugString("ERROR: cant connect to database")
	end

end
addEventHandler("onResourceStart", root, connectDatabase)

function disconnectDatabase()
	destroyElement(db)
	outputDebugString("Database disconnected")
end
addEventHandler("onResourceStop", root, disconnectDatabase)

--Auth functions
function isAccountInDB(Login)
	query = dbQuery(db, "SELECT COUNT(*) FROM players WHERE Login = ?", Login)
	result = dbPoll(query, -1)

	if result[1]["COUNT(*)"] == 1 then
		return true
	else
		return false
	end
end

function addAccountToDB(Login, Password, Serial)
	dbExec(db, "INSERT INTO players VALUES(NULL, ?, ?, ?, ?)", Login, Password, Serial, acc_START_MONEY)
end

function isPasswordCorrect(Login, Password)
	query = dbQuery(db, "SELECT Password FROM players WHERE Login = ?", Login)
	result = dbPoll(query, -1)

	local dbPassword = result[1]["Password"]

	if Password == dbPassword then
		return true
	else
		return false
	end
end
